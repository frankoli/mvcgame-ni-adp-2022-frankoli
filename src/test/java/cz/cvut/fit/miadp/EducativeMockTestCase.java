package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory_A;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EducativeMockTestCase {

    @Test
    public void createMissile() {
        IGameModel model = mock(GameModel.class);
        when(model.getCannonPosition()).thenReturn(new Position(new ArrayList<Integer>(Arrays.asList(555, 666))));
        when(model.getMovingStrategy()).thenReturn(new SimpleMovingStrategy());
        IGameObjectFactory goFact = new GameObjectFactory_A(model);
        AbsMissile missile = goFact.createMissile(0, 0);
        Assert.assertEquals(missile.getPosition().getCoordinate(X).get().intValue(), 555);
        Assert.assertEquals(missile.getPosition().getCoordinate(Y).get().intValue(), 666);
    }

}
