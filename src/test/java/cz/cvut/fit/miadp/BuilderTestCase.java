package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.builder.Director;
import cz.cvut.fit.miadp.mvcgame.builder.GameSettingsBuilder;
import cz.cvut.fit.miadp.mvcgame.model.enums.GameMode;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameSettings;
import org.junit.Assert;
import org.junit.Test;


public class BuilderTestCase {
    @Test
    public void createHardGameSettings() {
        Director director = new Director();
        GameSettingsBuilder builder = new GameSettingsBuilder();

        director.constructHardGame(builder);
        GameSettings settings = builder.getResult();

        Assert.assertEquals(settings.getMode(), GameMode.Hard);
        Assert.assertEquals(settings.getLevelsCount(), 10);
        Assert.assertEquals(settings.getMinScore(), 60);
        for (int i = 0; i < 10; i++) {
            Assert.assertEquals(3 + i, settings.getEnemiesCount(i));
            // enemy count is increase by 1 each level
            // 3 enemies in first level
        }
    }

    @Test
    public void createEasyGameSettings() {
        Director director = new Director();
        GameSettingsBuilder builder = new GameSettingsBuilder();

        director.constructEasyGame(builder);
        GameSettings settings = builder.getResult();

        Assert.assertEquals(settings.getMode(), GameMode.Easy);
        Assert.assertEquals(settings.getLevelsCount(), 1);
        Assert.assertEquals(settings.getMinScore(), 10);
        Assert.assertEquals(settings.getEnemiesCount(1), 4);
    }

    @Test
    public void createMediumGameSettings() {
        Director director = new Director();
        GameSettingsBuilder builder = new GameSettingsBuilder();
        director.constructMediumGame(builder);
        GameSettings settings = builder.getResult();

        Assert.assertEquals(settings.getMode(), GameMode.Medium);
        Assert.assertEquals(settings.getLevelsCount(), 5);
        Assert.assertEquals(settings.getMinScore(), 60);
        for (int i = 0; i < settings.getLevelsCount(); i++) {
            Assert.assertEquals(5, settings.getEnemiesCount(i));
            // 5 enemies in all levels
        }
    }
}

