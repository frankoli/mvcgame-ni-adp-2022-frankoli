package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.Missile_A;
import cz.cvut.fit.miadp.mvcgame.proxy.GameModelProxy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import junit.framework.TestCase;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class GameModelProxyTest extends TestCase {

    public void testUpdate() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.update();
        verify(model, times(1)).update();

    }

    public void testGetCannonPosition() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.getCannonPosition();
        verify(model, times(1)).getCannonPosition();

    }

    public void testMoveCannonUp() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.moveCannonUp();
        verify(model, times(1)).moveCannonUp();

    }


    public void testNextGameMode() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.nextGameMode();
        verify(model, times(1)).nextGameMode();

    }

    public void testMoveCannonDown() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.moveCannonDown();
        verify(model, times(1)).moveCannonDown();

    }

    public void testAimCannonUp() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.aimCannonUp();
        verify(model, times(1)).aimCannonUp();
    }

    public void testAimCannonDown() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.aimCannonDown();
        verify(model, times(1)).aimCannonDown();

    }

    public void testCannonPowerUp() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.cannonPowerUp();
        verify(model, times(1)).cannonPowerUp();

    }

    public void testCannonPowerDown() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.cannonPowerDown();
        verify(model, times(1)).cannonPowerDown();

    }

    public void testCannonShoot() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.cannonShoot();
        verify(model, times(1)).cannonShoot();

    }

    public void testGetMissiles() {
        IGameModel model = mock(GameModel.class);
        Missile_A missile_a = mock(Missile_A.class);
        when(model.getMissiles()).thenReturn(new ArrayList<>(Arrays.asList(missile_a)));
        GameModelProxy proxy = new GameModelProxy(model);
        List<AbsMissile> missiles = proxy.getMissiles();
        Assert.assertEquals(1, missiles.size());
        verify(model, times(1)).getMissiles();

    }

    public void testGetMovingStrategy() {
        IGameModel model = mock(GameModel.class);
        when(model.getMovingStrategy()).thenReturn(new SimpleMovingStrategy());
        GameModelProxy proxy = new GameModelProxy(model);
        Assert.assertEquals(SimpleMovingStrategy.class, proxy.getMovingStrategy().getClass());
        verify(model, times(1)).getMovingStrategy();

    }

    public void testToggleMovingStrategy() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.toggleMovingStrategy();
        verify(model, times(1)).toggleMovingStrategy();
    }

    public void testToggleShootingMode() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.toggleShootingMode();
        verify(model, times(1)).toggleShootingMode();
    }

    public void testIncreaseMissilesCount() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.increaseMissilesCount();
        verify(model, times(1)).increaseMissilesCount();
    }

    public void testDecreaseMissilesCount() {
        IGameModel model = mock(GameModel.class);
        GameModelProxy proxy = new GameModelProxy(model);
        proxy.decreaseMissilesCount();
        verify(model, times(1)).decreaseMissilesCount();

    }

    public void testIsSettingsMode() {
        IGameModel model = mock(GameModel.class);
        when(model.isSettingsMode()).thenReturn(false);
        GameModelProxy proxy = new GameModelProxy(model);
        Assert.assertFalse(proxy.isSettingsMode());
        verify(model, times(1)).isSettingsMode();
    }


    public void testGetLevel() {
        IGameModel model = mock(GameModel.class);
        when(model.getLevel()).thenReturn(2);
        GameModelProxy proxy = new GameModelProxy(model);
        Assert.assertEquals(2, proxy.getLevel());
        verify(model, times(1)).getLevel();
    }

}