package cz.cvut.fit.miadp;

import cz.cvut.fit.miadp.mvcgame.command.MoveCannonUpCmd;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import org.junit.Assert;
import org.junit.Test;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;

public class EducativeTestCase {

    @Test
    public void undoCommandTest() {
        IGameModel model = new GameModel();
        int positionBeforeUndoX = model.getCannonPosition().getCoordinate(X).get();
        int positionBeforeUndoY = model.getCannonPosition().getCoordinate(Y).get();
        var command = new MoveCannonUpCmd();
        command.setSubject(model);
        model.registerCommand(command);
        model.update();
        int positionAfterExcecution = model.getCannonPosition().getCoordinate(Y).get();
        model.undoLastCommand();
        int positionAfterUndoX = model.getCannonPosition().getCoordinate(X).get();
        int positionAfterUndoY = model.getCannonPosition().getCoordinate(Y).get();
        Assert.assertEquals(positionBeforeUndoY, positionAfterExcecution + MvcGameConfig.MOVE_STEP);
        Assert.assertEquals(positionBeforeUndoX, positionAfterUndoX);
        Assert.assertEquals(positionBeforeUndoY, positionAfterUndoY);
    }

}

