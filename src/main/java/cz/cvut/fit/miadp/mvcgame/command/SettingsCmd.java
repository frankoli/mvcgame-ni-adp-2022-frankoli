package cz.cvut.fit.miadp.mvcgame.command;

public class SettingsCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.gameSettings();
    }

    @Override
    public AbstractGameCommand copy() {
        SettingsCmd command = new SettingsCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "mode settings and back";
    }
}
