package cz.cvut.fit.miadp.mvcgame.factoryMethod;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.enums.EnemyType;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameSettings;

import java.util.ArrayList;
import java.util.Arrays;

public class FMethodEasyMode extends FMethod {
    @Override
    public ArrayList<AbsEnemy> createEnemies(IGameModel model) {
        GameSettings settings = model.getSettings();
        ArrayList<AbsEnemy> enemies = new ArrayList<AbsEnemy>();
        for (int i = 0; i < settings.getEnemiesCount(model.getLevel()); i++) {
            Position position = new Position(new ArrayList<Integer>(Arrays.asList(rand.nextInt(max_x) + 100, rand.nextInt(max_y))));
            enemies.add(model.getGoFact().createEnemy(EnemyType.second, position));
        }
        return enemies;
    }
}
