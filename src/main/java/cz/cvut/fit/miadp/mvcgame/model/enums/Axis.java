package cz.cvut.fit.miadp.mvcgame.model.enums;

public enum Axis {
    X,
    Y
}
