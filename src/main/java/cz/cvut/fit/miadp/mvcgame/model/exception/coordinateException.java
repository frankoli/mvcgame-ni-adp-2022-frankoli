package cz.cvut.fit.miadp.mvcgame.model.exception;

public class coordinateException extends Exception {
    public coordinateException(String errorMessage) {
        super(errorMessage);
    }
}