package cz.cvut.fit.miadp.mvcgame.command;

public class CannonPowerDownCmd extends AbstractGameCommand {

    @Override
    protected void execute() {
        this.subject.cannonPowerDown();
    }

    @Override
    public CannonPowerDownCmd copy() {
        CannonPowerDownCmd command = new CannonPowerDownCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Cannon power down";
    }
}
