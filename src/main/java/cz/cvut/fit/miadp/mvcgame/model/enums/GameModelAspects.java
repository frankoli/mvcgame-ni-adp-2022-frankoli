package cz.cvut.fit.miadp.mvcgame.model.enums;

public enum GameModelAspects {
    moveCannonUp,
    moveCanonDown,
    CanonShoot,
    moveMissiles,
    aimCannonDown,
    aimCannonUp,
    cannonPowerUp,
    cannonPowerDown,
    enemyDestroyed,
    collisionAdd
}
