package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.enums.GameModelAspects;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameSettings;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

import java.util.List;

public interface IGameModel extends IObservable<GameModelAspects> {
    void update();

    Position getCannonPosition();

    void moveCannonUp();

    void gameSettings();

    void nextGameMode();

    void moveCannonDown();

    void aimCannonUp();

    void aimCannonDown();

    void cannonPowerUp();

    void cannonPowerDown();

    void cannonShoot();

    List<AbsMissile> getMissiles();

    List<GameObject> getGameObjects();

    IMovingStrategy getMovingStrategy();

    void toggleMovingStrategy();

    void toggleShootingMode();

    void increaseMissilesCount();

    void decreaseMissilesCount();

    Object createMemento();

    void setMemento(Object memento);

    boolean isSettingsMode();

    GameSettings getSettings();

    int getLevel();

    IGameObjectFactory getGoFact();

    void registerCommand(AbstractGameCommand cmd);

    void undoLastCommand();

    void createControllerInfo(GameController controller);

    void reload();
}
