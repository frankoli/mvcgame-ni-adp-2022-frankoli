package cz.cvut.fit.miadp.mvcgame.command;

public class ToggleMovingStrategyCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.toggleMovingStrategy();
    }

    @Override
    public String getDescription() {
        return "Toggle moving strategy";
    }

    @Override
    public ToggleMovingStrategyCmd copy() {
        ToggleMovingStrategyCmd command = new ToggleMovingStrategyCmd();
        command.setSubject(this.subject);
        return command;
    }
}
