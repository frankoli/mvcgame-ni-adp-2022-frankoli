package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.DynamicShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.state.SingleShootingMode;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.List;

public abstract class AbsCannon extends GameObject {
    protected static IShootingMode SINGLE_SHOOTING_MODE = new SingleShootingMode();
    protected static IShootingMode DOUBLE_SHOOTING_MODE = new DoubleShootingMode();
    protected static IShootingMode DYNAMIC_SHOOTING_MODE = new DynamicShootingMode();
    protected static int missilesCount = 1;
    protected IShootingMode shootingMode;

    public String getShootingMode() {
        if (shootingMode == SINGLE_SHOOTING_MODE) {
            return "Single";
        } else if (shootingMode == DYNAMIC_SHOOTING_MODE) {
            return "Dynamic";
        } else if (shootingMode == DOUBLE_SHOOTING_MODE) {
            return "Double";
        }
        return "";
    }

    public int getMissilesCount() {
        if (shootingMode == SINGLE_SHOOTING_MODE) return 1;
        if (shootingMode == DOUBLE_SHOOTING_MODE) return 2;
        if (shootingMode == DYNAMIC_SHOOTING_MODE) return missilesCount;
        else return 0;
    }


    public abstract void moveUp();

    public abstract void moveDown();

    public abstract void aimUp();

    public abstract void aimDown();

    public abstract void powerUp();

    public abstract void powerDown();

    public abstract List<AbsMissile> shoot();

    public abstract void primitiveShoot();

    public abstract double getAngle();

    public abstract int getPower();

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitCannon(this);
    }

    public void toggleShootingMode() {
        if (this.shootingMode instanceof SingleShootingMode) {
            this.shootingMode = DOUBLE_SHOOTING_MODE;
        } else if (this.shootingMode instanceof DoubleShootingMode) {
            this.shootingMode = DYNAMIC_SHOOTING_MODE;
        } else if (this.shootingMode instanceof DynamicShootingMode) {
            this.shootingMode = SINGLE_SHOOTING_MODE;
            missilesCount = 1;
        } else {

        }
    }

    public void increaseMissilesCount() {
        if (this.shootingMode == DYNAMIC_SHOOTING_MODE && missilesCount < MvcGameConfig.MAX_MISSILE_COUNT) {
            missilesCount += 1;
        }
    }

    public void decreaseMissilesCount() {
        if (this.shootingMode == DYNAMIC_SHOOTING_MODE && missilesCount > 0) {
            missilesCount -= 1;
        }
    }


}
