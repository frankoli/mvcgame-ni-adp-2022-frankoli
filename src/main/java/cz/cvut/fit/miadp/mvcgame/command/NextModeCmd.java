package cz.cvut.fit.miadp.mvcgame.command;

public class NextModeCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.nextGameMode();
    }

    @Override
    public AbstractGameCommand copy() {
        NextModeCmd command = new NextModeCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Change game mode";
    }
}
