package cz.cvut.fit.miadp.mvcgame.command;

public class MoveCannonDownCmd extends AbstractGameCommand {

    public MoveCannonDownCmd() {
    }

    @Override
    protected void execute() {
        this.subject.moveCannonDown();
    }

    @Override
    public String getDescription() {
        return "Move cannon down";
    }

    @Override
    public MoveCannonDownCmd copy() {
        MoveCannonDownCmd command = new MoveCannonDownCmd();
        command.setSubject(this.subject);
        return command;
    }

}