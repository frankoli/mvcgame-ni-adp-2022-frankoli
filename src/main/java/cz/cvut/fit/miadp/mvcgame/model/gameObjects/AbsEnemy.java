package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.prototype.Copyable;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;


public abstract class AbsEnemy extends GameObject implements Copyable {
    protected static int idCounter = 0;
    protected int lives;
    protected int id;

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

    public int getId() {
        return id;
    }

    public int decreaseLives() {
        lives -= 1;
        return 1;
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitEnemy(this);
    }

    public abstract AbsEnemy copy();

    public abstract String getPicture();
}
