package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.prototype.Copyable;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsCollision extends LifetimeLimitedGameObject implements Copyable {
    protected AbsEnemy enemy;

    protected AbsCollision(AbsEnemy enemy) {
        super(enemy.getPosition());
        this.enemy = enemy;
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitCollision(this);
    }

    public abstract String getPicture();

    public abstract AbsCollision copy();
}
