package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

import java.time.LocalDateTime;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;

public class Enemy1_A extends AbsEnemy {

    //    private LocalDateTime bornAt;
    private LocalDateTime lastMove;
    private boolean up;

    public Enemy1_A(Position initialPosition) {
        this.id = idCounter++;
        lives = 3;
        this.position = initialPosition;
        this.up = true;
        this.lastMove = LocalDateTime.now();
    }

    public Position getPosition() {
        LocalDateTime now = LocalDateTime.now();

        if (now.isAfter(lastMove) && (lastMove.getMinute() * 60 + lastMove.getSecond() < now.getMinute() * 60 + now.getSecond())) {
            lastMove = LocalDateTime.now();
            if (up) {
                up = false;
                this.position.setCoordinate(Y, position.getCoordinate(Y).get() + 10);
            } else {
                up = true;
                this.position.setCoordinate(Y, position.getCoordinate(Y).get() - 10);
            }
        }
        return this.position;
    }

    public int decreaseLives() {
        lives -= 1;
        if (lives == 0) {
            return 10;
        } else {
            return 1;
        }
    }

    @Override
    public AbsEnemy copy() {
        Enemy1_A toReturn = new Enemy1_A(this.getPosition());
        toReturn.id = this.id;
        toReturn.setLives(lives);
        toReturn.up = this.up;
        toReturn.lastMove = this.lastMove;
        return toReturn;
    }

    @Override
    public String getPicture() {
        return "images2/enemy1.png";
    }
}
