package cz.cvut.fit.miadp.mvcgame.observer;

public interface IObserver<T extends Enum<?>> {

    void update();

    void update(T aspect);

}
