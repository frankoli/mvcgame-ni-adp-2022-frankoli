package cz.cvut.fit.miadp.mvcgame.builder;

import cz.cvut.fit.miadp.mvcgame.model.enums.GameMode;

public class Director {
    public void constructEasyGame(Builder builder) {
        builder.setGameMode(GameMode.Easy);
        builder.setLevelsCount(1);
        builder.setMinScore(10);
        builder.setEnemiesCount(4);
    }

    public void constructMediumGame(Builder builder) {
        builder.setGameMode(GameMode.Medium);
        builder.setLevelsCount(5);
        builder.setMinScore(60);
        builder.setEnemiesCount(5);

    }

    public void constructHardGame(Builder builder) {
        builder.setGameMode(GameMode.Hard);
        builder.setLevelsCount(10);
        builder.setMinScore(60);
        builder.setEnemiesCount(3);
        builder.setIncreaseEnemiesCount(1);
    }
}
