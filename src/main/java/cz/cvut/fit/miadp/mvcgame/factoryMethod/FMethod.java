package cz.cvut.fit.miadp.mvcgame.factoryMethod;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

import java.util.ArrayList;
import java.util.Random;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.MAX_X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.MAX_Y;

public abstract class FMethod {

    int max_x = MAX_X - 350;
    int max_y = MAX_Y - 100;
    Random rand = new Random();

    public abstract ArrayList<AbsEnemy> createEnemies(IGameModel model);
}
