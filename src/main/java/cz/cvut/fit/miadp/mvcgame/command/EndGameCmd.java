package cz.cvut.fit.miadp.mvcgame.command;

public class EndGameCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        System.exit(0);
    }

    @Override
    public EndGameCmd copy() {
        EndGameCmd command = new EndGameCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Close window";
    }
}
