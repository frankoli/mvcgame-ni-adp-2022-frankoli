package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;

public class GameGraphics implements IGameGraphics {

    IGameGraphicsImplementor implementor;

    public GameGraphics(IGameGraphicsImplementor implementor) {
        this.implementor = implementor;
    }

    @Override
    public void drawImage(String path, Position pos) {
        this.implementor.drawImage(path, pos);
    }

    @Override
    public void drawText(String text, Position pos, Color color) {
        this.implementor.drawText(text, pos, color);
    }

    @Override
    public void drawRectangle(Position leftTop, Position rightBottom) {
        ArrayList<Integer> position = new ArrayList<Integer>(Arrays.asList(rightBottom.getCoordinate(X).get(), leftTop.getCoordinate(Y).get()));
        ArrayList<Integer> position2 = new ArrayList<Integer>(Arrays.asList(leftTop.getCoordinate(X).get(), rightBottom.getCoordinate(Y).get()));
        this.implementor.drawLine(leftTop, new Position(position));
        this.implementor.drawLine(new Position(position), rightBottom);
        this.implementor.drawLine(rightBottom, new Position(position2));
        this.implementor.drawLine(new Position(position2), leftTop);
    }


    @Override
    public void clear() {
        this.implementor.clear();
    }

}
