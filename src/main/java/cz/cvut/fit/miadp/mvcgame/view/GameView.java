package cz.cvut.fit.miadp.mvcgame.view;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.enums.GameModelAspects;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.visitor.GameRenderer;
import cz.cvut.fit.miadp.mvcgame.visitor.GameSound;

import java.util.Set;

public class GameView implements IObserver<GameModelAspects> {

    private final GameController controller;
    private final IGameModel model;
    private final GameRenderer renderer;
    private final GameSound sounder;
    private IGameGraphics gr;

    public GameView(IGameModel model) {
        this.model = model;
        this.controller = new GameController(model);
        this.gr = null;
        this.model.registerObserver(this, Set.of(GameModelAspects.moveCannonUp, GameModelAspects.moveCanonDown,
                GameModelAspects.CanonShoot, GameModelAspects.moveMissiles, GameModelAspects.enemyDestroyed, GameModelAspects.collisionAdd));
        this.renderer = new GameRenderer();
        this.sounder = new GameSound();
    }

    public GameController getController() {
        return this.controller;
    }

    public void render() {
        if (this.gr == null) {
            return;
        }
        this.gr.clear();
        this.renderer.drawBackground();
        if (this.model.getGameObjects().size() == 0) {
            this.renderer.drawWin();
            this.sounder.playWin();
        }
        for (GameObject go : this.model.getGameObjects()) {
            go.acceptVisitor(this.renderer);
        }
    }

    public void playSound(GameModelAspects aspect) {
        for (GameObject go : this.model.getGameObjects()) {
            if (go instanceof AbsMissile && aspect == GameModelAspects.CanonShoot) {
                go.acceptVisitor(this.sounder);
            } else if (go instanceof AbsCannon && (aspect == GameModelAspects.moveCannonUp
                    || aspect == GameModelAspects.moveCanonDown)) {
                go.acceptVisitor(this.sounder);
            } else if (go instanceof AbsEnemy && aspect == GameModelAspects.enemyDestroyed) {
                go.acceptVisitor(this.sounder);
            } else if (go instanceof AbsCollision && aspect == GameModelAspects.collisionAdd) {
                go.acceptVisitor(this.sounder);
            }
        }
    }

    public void setGraphicContext(IGameGraphics gr) {
        boolean initCall = this.gr == null;
        this.gr = gr;
        this.renderer.setGraphicContext(gr);
        this.update();
    }

    @Override
    public void update() {
        this.render();
    }

    @Override
    public void update(GameModelAspects aspect) {
        this.render();
        this.playSound(aspect);
    }

}
