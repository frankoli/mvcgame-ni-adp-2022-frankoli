package cz.cvut.fit.miadp.mvcgame.builder;

import cz.cvut.fit.miadp.mvcgame.model.enums.GameMode;

public interface Builder {
    void setLevelsCount(int levels);

    void setGameMode(GameMode mode);

    void setMinScore(int score);

    void setEnemiesCount(int enemies);

    void setIncreaseEnemiesCount(int count);
}
