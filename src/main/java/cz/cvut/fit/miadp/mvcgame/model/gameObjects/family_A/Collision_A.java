package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

public class Collision_A extends AbsCollision {

    public Collision_A(AbsEnemy enemy) {
        super(enemy);
    }

    @Override
    public String getPicture() {
        if (this.enemy.getClass() == Enemy1_A.class) {
            return "images2/collison.png";
        } else if (this.enemy.getClass() == Enemy2_A.class) {
            return "images2/enemy2WithBlood.png";
        } else {
            return "";
        }
    }

    @Override
    public AbsCollision copy() {
        Collision_A toReturn = new Collision_A(this.enemy);
        return toReturn;
    }
}
