package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.HashMap;

public class ControlInfo extends GameObject {
    GameController controller;

    public ControlInfo(GameController controller) {
        this.controller = controller;
    }

    public HashMap<String, String> getInfo() {
        return controller.getKeysWithInfo();
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitControlInfo(this);
    }
}
