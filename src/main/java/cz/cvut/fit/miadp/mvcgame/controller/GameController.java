package cz.cvut.fit.miadp.mvcgame.controller;

import cz.cvut.fit.miadp.mvcgame.command.*;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class GameController {

    private final IGameModel model;
    private final HashMap<String, AbstractGameCommand> commands;

    public GameController(IGameModel model) {
        this.model = model;
        commands = new HashMap<>();
        commands.put("UP", new MoveCannonUpCmd());
        commands.put("DOWN", new MoveCannonDownCmd());
        commands.put("SPACE", new CannonShootCmd());
        commands.put("A", new AimCannonUpCmd());
        commands.put("Y", new AimCannonDownCmd());
        commands.put("F", new CannonPowerUpCmd());
        commands.put("D", new CannonPowerDownCmd());
        commands.put("M", new ToggleMovingStrategyCmd());
        commands.put("N", new ToggleShootingModeCmd());
        commands.put("V", new IncreaseMissilesCountCmd());
        commands.put("G", new DecreaseMissilesCountCmd());
        commands.put("S", new SettingsCmd());
        commands.put("RIGHT", new NextModeCmd());
        commands.put("ESCAPE", new EndGameCmd());
        commands.put("R", new ReloadGameCmd());
    }

    public void processPressedKeys(List<String> pressedKeysCodes) {
        for (String code : pressedKeysCodes) {
            for (String name : commands.keySet()) {
                if (Objects.equals(name, code)) {
                    AbstractGameCommand command = commands.get(name);
                    command.setSubject(this.model);
                    this.model.registerCommand(command.copy());
                }
            }
            if (Objects.equals(code, "B"))
                this.model.undoLastCommand();
        }
    }

    public HashMap<String, String> getKeysWithInfo() {
        HashMap<String, String> to_return = new HashMap<>();
        for (String name : commands.keySet()) {
            AbstractGameCommand command = commands.get(name);
            to_return.put(name, command.getDescription());
        }
        to_return.put("B", "go back");
        return to_return;
    }

}
