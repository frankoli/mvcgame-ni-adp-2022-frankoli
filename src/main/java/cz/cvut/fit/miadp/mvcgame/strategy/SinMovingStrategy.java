package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;


public class SinMovingStrategy implements IMovingStrategy {

    @Override
    public void updatePosition(AbsMissile missile) {
        double initAngle = missile.getInitAngle();
        int initVelocity = missile.getInitVelocity();
        long time = missile.getAge() / 100;

        int dX = (int) (initVelocity * time * Math.cos(initAngle));
        int dY = (int) (initVelocity * time * Math.sin(initAngle) + (pow((-1), time % 2)) * time * time);

        missile.move(new Vector(new ArrayList<>(List.of(dX, dY))));

    }

}