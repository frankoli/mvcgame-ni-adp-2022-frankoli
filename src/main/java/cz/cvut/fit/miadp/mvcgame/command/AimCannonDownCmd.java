package cz.cvut.fit.miadp.mvcgame.command;

public class AimCannonDownCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.aimCannonDown();
    }

    @Override
    public AimCannonDownCmd copy() {
        AimCannonDownCmd command = new AimCannonDownCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Aim cannon down";
    }
}
