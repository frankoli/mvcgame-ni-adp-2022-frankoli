package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.model.IGameModel;

public abstract class AbstractGameCommand {

    IGameModel subject;
    Object memento;

    public void setSubject(IGameModel subject) {
        this.subject = subject;
    }

    protected abstract void execute();

    public void doExecute() {
        if (this.getClass() == NextModeCmd.class || (this.getClass() == SettingsCmd.class && this.subject.isSettingsMode())) {
            this.memento = null;
        } else {
            this.memento = this.subject.createMemento();
        }
        this.execute();
    }

    public void unExecute() {
        this.subject.setMemento(this.memento);
    }

    public abstract AbstractGameCommand copy();

    public abstract String getDescription();

    public boolean hasMemento() {
        return this.memento != null;
    }
}
