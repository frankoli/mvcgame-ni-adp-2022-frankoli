package cz.cvut.fit.miadp.mvcgame.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

public class DynamicShootingMode implements IShootingMode {

    @Override
    public void shoot(AbsCannon cannon) {
        int countMissiles = cannon.getMissilesCount();
        if (countMissiles % 2 == 1) {
            cannon.primitiveShoot();
            countMissiles--;
        }
        for (int i = 0; i < countMissiles / 2; i++) {
            cannon.aimUp();
            cannon.primitiveShoot();
        }
        for (int i = 0; i < countMissiles / 2; i++) {
            cannon.aimDown();
        }
        for (int i = 0; i < countMissiles / 2; i++) {
            cannon.aimDown();
            cannon.primitiveShoot();
        }
        for (int i = 0; i < countMissiles / 2; i++) {
            cannon.aimUp();
        }

    }

    @Override
    public String getName() {
        return "DynamicShootingMode";
    }

}