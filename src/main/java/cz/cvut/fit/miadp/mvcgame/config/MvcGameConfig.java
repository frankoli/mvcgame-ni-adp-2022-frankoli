package cz.cvut.fit.miadp.mvcgame.config;

import java.util.ArrayList;
import java.util.List;

public class MvcGameConfig {
    public static final int MAX_X = 1280;
    public static final int MIN_X = 0;
    public static final int MISSILE_X_UP = 200;
    public static final int MISSILE_X_STRAIT = 250;
    public static final int MISSILE_X_DOWN = 450;
    public static final int MAX_Y = 720;
    public static final int MIN_Y = 0;
    public static final int X = 0;
    public static final int Y = 1;
    public static final int[] MIN_COR = {MIN_X, MIN_Y};
    public static final int[] MAX_COR = {MAX_X, MAX_Y};
    public static final int MOVE_STEP = 10;
    public static final ArrayList<Integer> CANNON_POS = new ArrayList<>(List.of(50, MAX_Y / 2));
    public static final ArrayList<Integer> CANNON_POS_B = new ArrayList<>(List.of(50, MAX_Y / 4));
    public static final int CANNON_POS_X = 50;
    public static final int CANNON_POS_Y = MAX_Y / 2;
    public static final int INIT_POWER = 10;
    public static final double INIT_ANGLE = 0;
    public static final double ANGLE_STEP = Math.PI / 18;
    public static final int POWER_STEP = 1;
    public static final int MAX_MISSILE_COUNT = 6;
    public static final double GRAVITY = 9.8;


}