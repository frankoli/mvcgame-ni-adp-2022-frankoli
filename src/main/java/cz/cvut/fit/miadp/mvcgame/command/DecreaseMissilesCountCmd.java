package cz.cvut.fit.miadp.mvcgame.command;

public class DecreaseMissilesCountCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.decreaseMissilesCount();
    }

    @Override
    public String getDescription() {
        return "Decrease missiles count";
    }

    @Override
    public DecreaseMissilesCountCmd copy() {
        DecreaseMissilesCountCmd command = new DecreaseMissilesCountCmd();
        command.setSubject(this.subject);
        return command;
    }
}
