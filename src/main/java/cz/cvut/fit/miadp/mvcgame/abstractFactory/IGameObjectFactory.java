package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.enums.EnemyType;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;

public interface IGameObjectFactory {
    AbsCannon createCannon();

    AbsMissile createMissile(double initAngle, int initVelocity);

    AbsEnemy createEnemy(EnemyType type, Position initPosition);

    GameInfo createGameInfo(GameModel model);

    AbsCollision createCollision(AbsEnemy enemy);
}
