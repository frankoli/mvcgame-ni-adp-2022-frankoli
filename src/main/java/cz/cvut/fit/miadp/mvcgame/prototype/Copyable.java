package cz.cvut.fit.miadp.mvcgame.prototype;

public interface Copyable {
    Object copy();
}
