package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;

public class JavaFxGraphics implements IGameGraphicsImplementor {

    private final GraphicsContext gr;

    public JavaFxGraphics(GraphicsContext gr) {
        this.gr = gr;
    }

    @Override
    public void drawImage(String path, Position pos) {
        Image image = new Image(path);
        this.gr.drawImage(image, pos.getCoordinate(X).get(), pos.getCoordinate(Y).get());
    }

    @Override
    public void drawText(String text, Position pos, Color color) {
        this.gr.setFill(color);
        this.gr.setTextAlign(TextAlignment.LEFT);
        this.gr.fillText(text, pos.getCoordinate(X).get(), pos.getCoordinate(Y).get());
    }

    @Override
    public void drawLine(Position beginPosition, Position endPosition) {
        this.gr.strokeLine(beginPosition.getCoordinate(X).get(), beginPosition.getCoordinate(Y).get(), endPosition.getCoordinate(X).get(), endPosition.getCoordinate(Y).get());
    }

    @Override
    public void clear() {
        this.gr.clearRect(0, 0, MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
    }

}