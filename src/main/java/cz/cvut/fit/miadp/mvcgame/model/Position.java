package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;

import java.util.ArrayList;
import java.util.Optional;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;

public class Position {
    private ArrayList<Integer> position = new ArrayList<>();

    public Position() {
    }

    public Position(Position copyOf) {
        this.position = new ArrayList<>(copyOf.position);
    }

    public Position(ArrayList<Integer> pos) {
        this.position = pos;
    }

    public boolean isPositionEqual(Position position) {
        if (this.getCoordinate(X).get() < (position.getCoordinate(X).get() + 20) &&
                this.getCoordinate(X).get() > (position.getCoordinate(X).get() - 20)) {
            return this.getCoordinate(Y).get() < (position.getCoordinate(Y).get() + 20) &&
                    this.getCoordinate(Y).get() > (position.getCoordinate(Y).get() - 20);
        }
        return false;
    }

    public Optional<Integer> getCoordinate(int index) {
        try {
            return Optional.ofNullable(position.get(index));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void setCoordinate(int index, int value) {
        if (index < position.size() && index >= 0) {
            position.set(index, value);
        }
//		else{
//			throw new coordinateException("Invalid index");
//		}
    }

    public void add(Vector vector) {
        for (int i = 0; i < position.size(); i++) {
            if (vector.getCoordinate(i).isEmpty()) {
                return;
            }
            if (position.get(i) + vector.getCoordinate(i).get() >= MvcGameConfig.MIN_COR[i]
                    && position.get(i) + vector.getCoordinate(i).get() <= MvcGameConfig.MAX_COR[i]) {
                position.set(i, position.get(i) + vector.getCoordinate(i).get());
            } else if (position.get(i) + vector.getCoordinate(i).get() < MvcGameConfig.MIN_COR[i]) {
                position.set(i, MvcGameConfig.MIN_COR[i]);
            } else if (position.get(i) + vector.getCoordinate(i).get() > MvcGameConfig.MAX_COR[i]) {
                position.set(i, MvcGameConfig.MAX_COR[i]);
            }
        }
    }


}