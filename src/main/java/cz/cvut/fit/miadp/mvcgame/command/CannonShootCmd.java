package cz.cvut.fit.miadp.mvcgame.command;

public class CannonShootCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.cannonShoot();
    }

    @Override
    public String getDescription() {
        return "Cannon shoot";
    }

    @Override
    public CannonShootCmd copy() {
        CannonShootCmd command = new CannonShootCmd();
        command.setSubject(this.subject);
        return command;
    }
}
