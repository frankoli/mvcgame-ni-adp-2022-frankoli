package cz.cvut.fit.miadp.mvcgame.command;

public class CannonPowerUpCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.cannonPowerUp();
    }

    @Override
    public CannonPowerUpCmd copy() {
        CannonPowerUpCmd command = new CannonPowerUpCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Cannon power up";
    }
}
