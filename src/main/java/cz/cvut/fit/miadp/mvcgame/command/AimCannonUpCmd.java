package cz.cvut.fit.miadp.mvcgame.command;

public class AimCannonUpCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.aimCannonUp();
    }

    @Override
    public AimCannonUpCmd copy() {
        AimCannonUpCmd command = new AimCannonUpCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Aim cannon up";
    }
}
