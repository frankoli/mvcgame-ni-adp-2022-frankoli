package cz.cvut.fit.miadp.mvcgame.command;

public class MoveCannonUpCmd extends AbstractGameCommand {

    @Override
    protected void execute() {
        this.subject.moveCannonUp();
    }

    @Override
    public String getDescription() {
        return "Move cannon up";
    }

    @Override
    public MoveCannonUpCmd copy() {
        MoveCannonUpCmd command = new MoveCannonUpCmd();
        command.setSubject(this.subject);
        return command;
    }
}
