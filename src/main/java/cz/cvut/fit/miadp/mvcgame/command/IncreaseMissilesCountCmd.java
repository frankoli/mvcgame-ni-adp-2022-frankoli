package cz.cvut.fit.miadp.mvcgame.command;

public class IncreaseMissilesCountCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.increaseMissilesCount();
    }

    @Override
    public String getDescription() {
        return "Increase missiles count";
    }

    @Override
    public IncreaseMissilesCountCmd copy() {
        IncreaseMissilesCountCmd command = new IncreaseMissilesCountCmd();
        command.setSubject(this.subject);
        return command;
    }
}
