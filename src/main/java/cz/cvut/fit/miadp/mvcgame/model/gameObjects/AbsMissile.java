package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public abstract class AbsMissile extends LifetimeLimitedGameObject {

    private final double initAngle;
    private final int initVelocity;
    private boolean shouldBeDestroyed;

    protected AbsMissile(Position initialPosition, double initAngle, int initVelocity) {
        super(initialPosition);
        this.initAngle = initAngle;
        this.initVelocity = initVelocity;
        this.shouldBeDestroyed = false;
    }

    public boolean isShouldBeDestroyed() {
        return shouldBeDestroyed;
    }

    public void setShouldBeDestroyed(boolean shouldBeDestroyed) {
        this.shouldBeDestroyed = shouldBeDestroyed;
    }

    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitMissile(this);

    }

    public int getInitVelocity() {
        return this.initVelocity;
    }

    public double getInitAngle() {
        return this.initAngle;
    }

    public abstract void move();


//    public abstract AbsCollision collision(AbsEnemy enemy);
}
