package cz.cvut.fit.miadp.mvcgame.model.enums;

public enum GameMode {
    Easy,
    Medium,
    Hard;

    public String toString(GameMode mode) {
        if (mode == GameMode.Easy) {
            return "Easy";
        } else if (mode == GameMode.Medium) {
            return "Medium";
        }
        return "Hard";
    }
}


