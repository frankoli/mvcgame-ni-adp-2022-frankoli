package cz.cvut.fit.miadp.mvcgame.command;

public class ToggleShootingModeCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.toggleShootingMode();
    }

    @Override
    public String getDescription() {
        return "Toggle shooting mode";
    }

    @Override
    public ToggleShootingModeCmd copy() {
        ToggleShootingModeCmd command = new ToggleShootingModeCmd();
        command.setSubject(this.subject);
        return command;
    }
}
