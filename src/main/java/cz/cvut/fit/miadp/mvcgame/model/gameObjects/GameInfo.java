package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

import java.util.HashMap;

public abstract class GameInfo extends GameObject {
    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitGameInfo(this);
    }

    public abstract HashMap<String, String> getInfo();
}
