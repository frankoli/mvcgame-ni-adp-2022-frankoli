package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;

public class Enemy2_A extends AbsEnemy {

    public Enemy2_A(Position initialPosition) {
        lives = 2;
        this.position = initialPosition;
    }

    public int decreaseLives() {
        lives -= 1;
        if (lives == 0) {
            return 5;
        } else {
            return 1;
        }
    }

    @Override
    public AbsEnemy copy() {
        Enemy2_A toReturn = new Enemy2_A(this.getPosition());
        toReturn.setLives(lives);
        toReturn.id = this.id;
        return toReturn;
    }

    @Override
    public String getPicture() {
        return "images2/enemy2.png";
    }
}
