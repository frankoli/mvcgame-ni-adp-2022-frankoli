package cz.cvut.fit.miadp.mvcgame.observer;

import java.util.Set;

public interface IObservable<T extends Enum<?>> {

    void registerObserver(IObserver obs, Set<T> aspects);

    void addAspect(IObserver obs, T aspect);

    void unregisterObserver(IObserver obs);

    void removeAspect(IObserver obs, T aspect);

    void notifyObservers(T aspect);

}

