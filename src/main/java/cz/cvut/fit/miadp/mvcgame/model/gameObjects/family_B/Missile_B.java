package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

public class Missile_B extends AbsMissile {

    public Missile_B(Position initialPosition, double initAngle, int initVelocity, IMovingStrategy movingStrategy) {
        super(initialPosition, initAngle, initVelocity);
        this.position = initialPosition;
    }

    @Override
    public void move(Vector vector) {
        int x = this.position.getCoordinate(0).get();
        if (x < MvcGameConfig.MISSILE_X_UP) {
            vector.setCoordinate(1, vector.getCoordinate(1).get() - 2);
        } else if (x < MvcGameConfig.MISSILE_X_STRAIT) {
            this.position.add(vector);
            return;
        } else if (x < MvcGameConfig.MISSILE_X_DOWN) {
            vector.setCoordinate(1, vector.getCoordinate(1).get() + 2);
        } else {
            vector.setCoordinate(1, vector.getCoordinate(1).get() + 4);
        }
        this.position.add(vector);
    }

    @Override
    public void move() {

    }
}
