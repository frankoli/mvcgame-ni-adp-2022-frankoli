package cz.cvut.fit.miadp.mvcgame.abstractFactory;


import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.enums.EnemyType;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.*;

public class GameObjectFactory_A implements IGameObjectFactory {
    private final IGameModel model;

    public GameObjectFactory_A(IGameModel model) {
        this.model = model;
    }


    @Override
    public Cannon_A createCannon() {
        return new Cannon_A(new Position(MvcGameConfig.CANNON_POS), this);
    }

    @Override
    public Missile_A createMissile(double initAngle, int initVelocity) {
        return new Missile_A(
                new Position(
                        model.getCannonPosition()
                ),
                initAngle,
                initVelocity,
                this.model.getMovingStrategy()
        );
    }

    @Override
    public AbsEnemy createEnemy(EnemyType type, Position initPosition) {
        if (type == EnemyType.first) {
            return new Enemy1_A(initPosition);
        }
        return new Enemy2_A(initPosition);
    }

    @Override
    public GameInfo createGameInfo(GameModel model) {
        return new GameInfo_A(model);
    }

    @Override
    public AbsCollision createCollision(AbsEnemy enemy) {
        return new Collision_A(enemy);
    }


}
