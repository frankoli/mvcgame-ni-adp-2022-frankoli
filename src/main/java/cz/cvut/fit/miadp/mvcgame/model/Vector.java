package cz.cvut.fit.miadp.mvcgame.model;

import java.util.ArrayList;
import java.util.Optional;

public class Vector {
    private ArrayList<Integer> position = new ArrayList<>();

    public Vector() {

    }

    public Vector(ArrayList<Integer> pos) {
        this.position = pos;
    }

    public Optional<Integer> getCoordinate(int index) {
        try {
            return Optional.ofNullable(position.get(index));
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    public void setCoordinate(int index, int value) {
        if (index < position.size() && index >= 0) {
            position.set(index, value);
        }
//        else{
//            throw new coordinateException("Invalid index");
//        }
    }
}
