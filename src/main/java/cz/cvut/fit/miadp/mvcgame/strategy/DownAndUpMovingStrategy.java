package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

import java.util.ArrayList;
import java.util.List;

public class DownAndUpMovingStrategy implements IMovingStrategy {

    @Override
    public void updatePosition(AbsMissile missile) {
        double initAngle = missile.getInitAngle();
        int initVelocity = missile.getInitVelocity();
        long time = missile.getAge() / 100;

        int dX = (int) (initVelocity * time * Math.cos(initAngle));
        int dY = (int) (initVelocity * time * Math.sin(initAngle) + (0.4 * MvcGameConfig.GRAVITY * time * time));
        if (missile.getPosition().getCoordinate(MvcGameConfig.X).get() > MvcGameConfig.MAX_X / 4)
            dY = (int) (initVelocity * time * Math.sin(initAngle) - (0.5 * MvcGameConfig.GRAVITY * time * time));
        if (missile.getPosition().getCoordinate(MvcGameConfig.X).get() > 2 * MvcGameConfig.MAX_X / 4)
            dY = (int) (initVelocity * time * Math.sin(initAngle));


        missile.move(new Vector(new ArrayList<>(List.of(dX, dY))));
    }

}