package cz.cvut.fit.miadp.mvcgame.builder;

import cz.cvut.fit.miadp.mvcgame.model.enums.GameMode;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameSettings;

public class GameSettingsBuilder implements Builder {
    protected GameMode mode;
    protected int levelsCount;
    protected int minScore;
    protected int enemies;
    protected int increaseEnemiesCount = 0;

    @Override
    public void setLevelsCount(int levels) {
        this.levelsCount = levels;
    }

    @Override
    public void setGameMode(GameMode mode) {
        this.mode = mode;
    }

    @Override
    public void setMinScore(int score) {
        this.minScore = score;
    }

    @Override
    public void setEnemiesCount(int enemies) {
        this.enemies = enemies;
    }

    @Override
    public void setIncreaseEnemiesCount(int count) {
        increaseEnemiesCount = count;
    }

    public GameSettings getResult() {
        return new GameSettings(mode, levelsCount, minScore, enemies, increaseEnemiesCount);
    }
}
