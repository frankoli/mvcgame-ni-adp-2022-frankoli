package cz.cvut.fit.miadp.mvcgame.command;

public class ReloadGameCmd extends AbstractGameCommand {
    @Override
    protected void execute() {
        this.subject.reload();
    }

    @Override
    public ReloadGameCmd copy() {
        ReloadGameCmd command = new ReloadGameCmd();
        command.setSubject(this.subject);
        return command;
    }

    @Override
    public String getDescription() {
        return "Reload game";
    }
}
