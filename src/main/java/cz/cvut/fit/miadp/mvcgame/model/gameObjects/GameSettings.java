package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.enums.GameMode;
import cz.cvut.fit.miadp.mvcgame.prototype.Copyable;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

public class GameSettings extends GameObject implements Copyable {
    private final GameMode mode;
    private final int levelsCount;
    private final int minScore;
    private final int enemiesCount;
    private final int increaseEnemiesCount;
    private int currentLevel;

    public GameSettings(GameMode mode, int levels, int score, int enemies, int increaseEnemiesCount) {
        this.mode = mode;
        this.levelsCount = levels;
        this.minScore = score;
        this.enemiesCount = enemies;
        this.increaseEnemiesCount = increaseEnemiesCount;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public int getEnemiesCount(int level) {
        currentLevel = level;
        return enemiesCount + level * increaseEnemiesCount;
    }

    public GameMode getMode() {
        return mode;
    }


    public int getLevelsCount() {
        return levelsCount;
    }

    public int getMinScore() {
        return minScore;
    }


    @Override
    public void acceptVisitor(IVisitor visitor) {
        visitor.visitSettings(this);
    }

    @Override
    public GameSettings copy() {
        GameSettings toReturn = new GameSettings(this.mode, this.levelsCount, this.minScore, this.enemiesCount, this.increaseEnemiesCount);
        toReturn.currentLevel = this.currentLevel;
        return toReturn;
    }
}
