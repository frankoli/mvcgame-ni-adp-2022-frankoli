package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjectFactory_A;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.builder.Director;
import cz.cvut.fit.miadp.mvcgame.builder.GameSettingsBuilder;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.factoryMethod.FMethod;
import cz.cvut.fit.miadp.mvcgame.factoryMethod.FMethodEasyMode;
import cz.cvut.fit.miadp.mvcgame.factoryMethod.FMethodHardMode;
import cz.cvut.fit.miadp.mvcgame.factoryMethod.FMethodMediumMode;
import cz.cvut.fit.miadp.mvcgame.model.enums.GameMode;
import cz.cvut.fit.miadp.mvcgame.model.enums.GameModelAspects;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.strategy.*;

import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.X;
import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.Y;
import static cz.cvut.fit.miadp.mvcgame.model.enums.GameModelAspects.*;

public class GameModel implements IGameModel {
    private final AbsCannon cannon;
    private List<AbsMissile> missiles;
    private final GameInfo gameInfo;
    private ControlInfo controlInfo;
    private List<AbsEnemy> enemies;
    private Map<Integer, AbsCollision> collisions;
    private Map<IObserver, Set<GameModelAspects>> observers;
    private final IGameObjectFactory goFact;
    private IMovingStrategy movingStrategy;
    private Queue<AbstractGameCommand> unExecutedCmds;
    private Stack<AbstractGameCommand> executedCmds;
    private GameSettings settings;
    private boolean settingsMode;
    private final Director director;
    private final GameSettingsBuilder gameBuilder;
    private int level;
    private int score;

    public GameModel() {
        this.director = new Director();
        this.gameBuilder = new GameSettingsBuilder();
        this.goFact = new GameObjectFactory_A(this);
        this.cannon = this.goFact.createCannon();
        this.gameInfo = this.goFact.createGameInfo(this);
        this.movingStrategy = new SimpleMovingStrategy();
        this.observers = new HashMap<>();
        setEmptyGameObjects();
        this.unExecutedCmds = new LinkedBlockingQueue<>();
        this.executedCmds = new Stack<>();
        director.constructMediumGame(gameBuilder);
        this.settings = gameBuilder.getResult();
        this.settingsMode = false;
        createEnemies(); //factory method
    }

    public IGameObjectFactory getGoFact() {
        return goFact;
    }

    public GameSettings getSettings() {
        return settings;
    }

    public boolean isSettingsMode() {
        return settingsMode;
    }

    public int getLevel() {
        return level;
    }

    public int getCount_enemies() {
        return enemies.size();
    }

    public int getScore() {
        return score;
    }

    public int getMissilesCount() {
        return cannon.getMissilesCount();
    }

    public String getCannonMode() {
        return cannon.getShootingMode();
    }

    public double getCannonAngle() {
        return cannon.getAngle();
    }

    public int getCannonPower() {
        return cannon.getPower();
    }

    // factory method design pattern
    void createEnemies() {
        FMethod method;
        if (settings.getMode() == GameMode.Easy) {
            method = new FMethodEasyMode();
        } else if (settings.getMode() == GameMode.Medium) {
            method = new FMethodMediumMode();
        } else {
            method = new FMethodHardMode();
        }
        this.enemies = method.createEnemies(this);
    }

    private void setEmptyGameObjects() {
        this.missiles = new ArrayList<>();
        this.collisions = new HashMap<>();
        this.score = 0;
        this.level = 0;
    }

    public void update() {
        this.executedCmds();
        this.moveMissiles();
    }

    private void executedCmds() {
        while (!this.unExecutedCmds.isEmpty()) {
            AbstractGameCommand cmd = this.unExecutedCmds.poll();
            cmd.doExecute();
            this.executedCmds.push(cmd);
        }
    }

    private void destroyEnemy(List<AbsEnemy> enemyToRemove) {
        Set<Integer> collisionToRemove = new HashSet<Integer>();
        for (AbsEnemy enemy : enemyToRemove) {
            for (Integer key_enemy : this.collisions.keySet()) {
                if (key_enemy == enemy.getId()) {
                    collisionToRemove.add(key_enemy);
                }
            }
        }
        for (Integer id : collisionToRemove) {
            this.collisions.remove(id);
            notifyObservers(enemyDestroyed);
        }
        this.enemies.removeAll(enemyToRemove);
        if (enemies.size() == 0) {
            level++;
            createEnemies();
        }
    }

    private void moveMissiles() {
        for (AbsMissile missile : this.missiles) {
            List<AbsEnemy> enemyToRemove = new ArrayList<AbsEnemy>();
            for (AbsEnemy enemy : enemies) {
                if (missile.getPosition().isPositionEqual(enemy.getPosition())) {
                    missile.setShouldBeDestroyed(true);
                    this.score += enemy.decreaseLives();
                    this.collisions.put(enemy.getId(), goFact.createCollision(enemy));
                    notifyObservers(collisionAdd);
                    if (enemy.getLives() == 0) {
                        enemyToRemove.add(enemy);
                    }
                    break;
                }
            }
            destroyEnemy(enemyToRemove);
            missile.move();
        }
        this.destroyMissiles();
        this.notifyObservers(GameModelAspects.moveMissiles);
    }

    private void destroyMissiles() {
        List<AbsMissile> missilesToRemove = new ArrayList<AbsMissile>();
        for (AbsMissile missile : this.missiles) {
            if (missile.getPosition().getCoordinate(0).get() > MvcGameConfig.MAX_X || missile.isShouldBeDestroyed()) {
                missilesToRemove.add(missile);
            }
        }
        this.missiles.removeAll(missilesToRemove);
    }


    public Position getCannonPosition() {
        return this.cannon.getPosition();
    }

    public void moveCannonUp() {
        if (!settingsMode) {
            this.cannon.moveUp();
            this.notifyObservers(moveCannonUp);
        }
    }

    @Override
    public void gameSettings() {
        this.settingsMode = !this.settingsMode;
        if (!this.settingsMode) {
            setEmptyGameObjects();
            createEnemies();
        }
    }

    @Override
    public void nextGameMode() {
        if (this.settingsMode) {
            if (this.settings.getMode() == GameMode.Easy) {
                director.constructMediumGame(gameBuilder);
            } else if (this.settings.getMode() == GameMode.Medium) {
                director.constructHardGame(gameBuilder);
            } else if (this.settings.getMode() == GameMode.Hard) {
                director.constructEasyGame(gameBuilder);
            }
            this.settings = gameBuilder.getResult();
        }
    }

    public void moveCannonDown() {
        if (!this.settingsMode) {
            this.cannon.moveDown();
            this.notifyObservers(moveCanonDown);
        }
    }

    public void aimCannonUp() {
        if (!this.settingsMode) {
            this.cannon.aimUp();
            this.notifyObservers(GameModelAspects.aimCannonUp);
        }
    }

    public void aimCannonDown() {
        if (!this.settingsMode) {
            this.cannon.aimDown();
            this.notifyObservers(GameModelAspects.aimCannonDown);
        }
    }

    public void cannonPowerUp() {
        if (!this.settingsMode) {
            this.cannon.powerUp();
            this.notifyObservers(GameModelAspects.cannonPowerUp);
        }
    }

    public void cannonPowerDown() {
        if (!this.settingsMode) {
            this.cannon.powerDown();
            this.notifyObservers(GameModelAspects.aimCannonDown);
        }
    }

    @Override
    public void registerObserver(IObserver obs, Set<GameModelAspects> aspects) {
        if (!this.observers.containsKey(obs)) {
            this.observers.put(obs, aspects);
        }
    }

    @Override
    public void addAspect(IObserver obs, GameModelAspects aspect) {
        if (this.observers.containsKey(obs)) {
            Set<GameModelAspects> aspects = this.observers.get(obs);
            aspects.add(aspect);
            this.observers.put(obs, aspects);
        }
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        this.observers.remove(obs);
    }

    @Override
    public void removeAspect(IObserver obs, GameModelAspects aspect) {
        if (this.observers.containsKey(obs)) {
            Set<GameModelAspects> aspects = this.observers.get(obs);
            aspects.remove(aspect);
            this.observers.put(obs, aspects);
        }
    }

    @Override
    public void notifyObservers(GameModelAspects aspect) {
        for (IObserver obs : this.observers.keySet()) {
            if (this.observers.get(obs).contains(aspect)) {
                obs.update(aspect);
            }
        }
    }

    public void cannonShoot() {
        if (!this.settingsMode) {
            this.missiles.addAll(cannon.shoot());
            this.notifyObservers(CanonShoot);
        }
    }

    public List<AbsMissile> getMissiles() {
        return this.missiles;
    }

    public List<GameObject> getGameObjects() {
        List<GameObject> go = new ArrayList<GameObject>();
        if (level == settings.getLevelsCount()) {
            return go;
        }
        go.add(this.cannon);
        go.addAll(this.missiles);
        go.addAll(this.enemies);
        go.addAll(this.collisions.values());
        go.add(this.gameInfo);
        go.add(this.controlInfo);
        go.add(this.settings);
        return go;
    }

    public IMovingStrategy getMovingStrategy() {
        return this.movingStrategy;
    }

    public void toggleMovingStrategy() {
        if (!this.settingsMode) {
            if (this.movingStrategy instanceof SimpleMovingStrategy) {
                this.movingStrategy = new RealisticMovingStrategy();
            } else if (this.movingStrategy instanceof RealisticMovingStrategy) {
                this.movingStrategy = new SinMovingStrategy();
            } else if (this.movingStrategy instanceof SinMovingStrategy) {
                this.movingStrategy = new DownAndUpMovingStrategy();
            } else if (this.movingStrategy instanceof DownAndUpMovingStrategy) {
                this.movingStrategy = new SimpleMovingStrategy();
            }
        }

    }

    public void toggleShootingMode() {
        if (!this.settingsMode) {
            this.cannon.toggleShootingMode();
        }
    }

    public void increaseMissilesCount() {
        if (!this.settingsMode) {
            this.cannon.increaseMissilesCount();
        }
    }

    public void decreaseMissilesCount() {
        if (!this.settingsMode) {
            this.cannon.decreaseMissilesCount();
        }
    }

    public Object createMemento() {
        Memento m = new Memento();
        m.levels = this.level;
        m.score = this.score;
        m.settings = this.settings.copy();
        m.cannonX = this.getCannonPosition().getCoordinate(X).get();
        m.cannonY = this.getCannonPosition().getCoordinate(Y).get();
        m.enemies = this.enemies.stream().map(obj -> obj.copy()).collect(Collectors.toList());
        m.collisions = this.collisions.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().copy()
                ));
        return m;
    }

    public void setMemento(Object memento) {
        Memento m = (Memento) memento;
        this.score = m.score;
        this.cannon.getPosition().setCoordinate(X, m.cannonX);
        this.cannon.getPosition().setCoordinate(Y, m.cannonY);
        this.enemies = m.enemies;
        this.collisions = m.collisions;
        this.level = m.levels;
        this.settings = m.settings;
    }

    @Override
    public void registerCommand(AbstractGameCommand cmd) {
        this.unExecutedCmds.add(cmd);
    }

    @Override
    public void undoLastCommand() {
        while (true) {
            if (!this.executedCmds.isEmpty()) {
                AbstractGameCommand cmd = this.executedCmds.pop();
                if (cmd.hasMemento()) {
                    cmd.unExecute();
                    break;
                }
            }
        }
        this.notifyObservers(moveCannonUp);
        this.notifyObservers(moveCanonDown);
        this.notifyObservers(CanonShoot);
        this.notifyObservers(moveMissiles);
        this.notifyObservers(aimCannonDown);
        this.notifyObservers(aimCannonUp);
        this.notifyObservers(cannonPowerUp);
        this.notifyObservers(cannonPowerDown);
    }

    @Override
    public void createControllerInfo(GameController controller) {
        this.controlInfo = new ControlInfo(controller);
    }

    @Override
    public void reload() {
        this.movingStrategy = new SimpleMovingStrategy();
        this.observers = new HashMap<>();
        setEmptyGameObjects();
        this.unExecutedCmds = new LinkedBlockingQueue<>();
        this.executedCmds = new Stack<>();
        director.constructMediumGame(gameBuilder);
        this.settings = gameBuilder.getResult();
        this.settingsMode = false;
        createEnemies(); //factory method
    }

    private class Memento {
        private int score;
        private int cannonX;
        private int cannonY;
        private int levels;
        private GameSettings settings;
        private List<AbsEnemy> enemies;
        private Map<Integer, AbsCollision> collisions;
        // GO positions
    }


}