package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.*;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig.*;

public class GameRenderer implements IVisitor {

    private IGameGraphics gr;

    public void setGraphicContext(IGameGraphics gr) {
        this.gr = gr;
    }

    public void drawBackground() {
        Position pos = new Position(new ArrayList<Integer>(Arrays.asList(MIN_X, MIN_Y)));
        this.gr.drawImage("images2/back_copy.jpg", pos);
    }

    public void drawWin() {
        Position pos = new Position(new ArrayList<Integer>(Arrays.asList(MAX_X / 2 - 200, MAX_Y / 2 - 130)));
        this.gr.drawImage("images2/win.png", pos);
    }

    @Override
    public void visitCannon(AbsCannon cannon) {
        this.gr.drawImage("images2/cannon.png", cannon.getPosition());
    }

    @Override
    public void visitMissile(AbsMissile missile) {
        this.gr.drawImage("images2/missile.png", missile.getPosition());
    }

    @Override
    public void visitEnemy(AbsEnemy enemy) {
        this.gr.drawImage(enemy.getPicture(), enemy.getPosition());
    }

    @Override
    public void visitGameInfo(GameInfo gameInfo) {
        int start_x = MIN_X + 20;
        int start_y = MIN_Y + 50;

        Position end = new Position(new ArrayList<Integer>(Arrays.asList(start_x + 150, start_y)));

        HashMap<String, String> info = gameInfo.getInfo();
        for (String name : info.keySet()) {
            start_y = start_y + 15;
            Position first = new Position(new ArrayList<Integer>(Arrays.asList(start_x + 5, start_y)));
            Position second = new Position(new ArrayList<Integer>(Arrays.asList(start_x + 100,
                    start_y)));
            this.gr.drawText(name, first, Color.BLACK);
            this.gr.drawText(info.get(name), second, Color.MAGENTA);
        }
        Position beg = new Position(new ArrayList<Integer>(Arrays.asList(start_x, start_y + 20)));
        this.gr.drawRectangle(beg, end);
    }

    @Override
    public void visitControlInfo(ControlInfo controlInfo) {
        HashMap<String, String> info = controlInfo.getInfo();
        int start_x = MAX_X - 200;
        int start_y = MIN_Y + 50;
        for (String name : info.keySet()) {
            start_y = start_y + 15;
            Position first = new Position(new ArrayList<Integer>(Arrays.asList(start_x + 5, start_y)));
            Position second = new Position(new ArrayList<Integer>(Arrays.asList(start_x + 5 + name.length() * 10 + 5,
                    start_y)));
            this.gr.drawText(name, first, Color.MAGENTA);
            this.gr.drawText(info.get(name), second, Color.CHOCOLATE);
        }
        //doleva             /// dolu rozsireni
        Position beg = new Position(new ArrayList<Integer>(Arrays.asList(start_x, start_y + 20)));
        Position end = new Position(new ArrayList<Integer>(Arrays.asList(MAX_X - 40, MIN_Y + 50)));
        this.gr.drawRectangle(beg, end);
    }

    @Override
    public void visitCollision(AbsCollision collision) {
        this.gr.drawImage(collision.getPicture(), collision.getPosition());
    }

    private Position getNewPosition(int x, int y) {
        return new Position(new ArrayList<Integer>(Arrays.asList(x, y)));
    }

    @Override
    public void visitSettings(GameSettings gameSettings) {
        Position beg = new Position(new ArrayList<Integer>(Arrays.asList(MAX_X - 200, MIN_Y + 400)));
        Position end = new Position(new ArrayList<Integer>(Arrays.asList(MAX_X - 40, MIN_Y + 500)));
        this.gr.drawRectangle(beg, end);
        int start_x = MAX_X - 200 + 5;
        int start_y = MIN_Y + 400 + 15;
        this.gr.drawText("Mode", getNewPosition(start_x, start_y), Color.MAROON);
        this.gr.drawText(gameSettings.getMode().toString(gameSettings.getMode()),
                getNewPosition(start_x + 70, start_y), Color.SNOW);
        this.gr.drawText("Score", getNewPosition(start_x, start_y + 15), Color.MAROON);
        this.gr.drawText(Integer.toString(gameSettings.getMinScore()),
                getNewPosition(start_x + 70, start_y + 15), Color.SNOW);
        this.gr.drawText("Enemies", getNewPosition(start_x, start_y + 30), Color.MAROON);
        this.gr.drawText(Integer.toString(gameSettings.getEnemiesCount(gameSettings.getCurrentLevel())),
                getNewPosition(start_x + 70, start_y + 30), Color.SNOW);
        this.gr.drawText("Levels", getNewPosition(start_x, start_y + 45), Color.MAROON);
        this.gr.drawText(Integer.toString(gameSettings.getLevelsCount()),
                getNewPosition(start_x + 70, start_y + 45), Color.SNOW);

    }

}

