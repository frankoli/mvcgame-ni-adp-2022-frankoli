package cz.cvut.fit.miadp.mvcgame.proxy;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.command.AbstractGameCommand;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.enums.GameModelAspects;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameSettings;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static cz.cvut.fit.miadp.mvcgame.model.enums.GameModelAspects.*;

public class GameModelProxy implements IGameModel {

    private final IGameModel subject;
    private final Set<GameModelAspects> aspekts = new HashSet<>(Arrays.asList(moveCannonUp, moveCanonDown, CanonShoot,
            moveMissiles, aimCannonDown, aimCannonUp, cannonPowerUp, cannonPowerDown));

    public GameModelProxy(IGameModel model) {
        this.subject = model;
    }

    @Override
    public void registerObserver(IObserver obs, Set<GameModelAspects> aspects) {
        this.subject.registerObserver(obs, aspects);
    }

    @Override
    public void addAspect(IObserver obs, GameModelAspects aspect) {
        this.subject.addAspect(obs, aspect);
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        this.subject.unregisterObserver(obs);
    }

    @Override
    public void removeAspect(IObserver obs, GameModelAspects aspect) {
        this.subject.removeAspect(obs, aspect);
    }

    @Override
    public void notifyObservers(GameModelAspects aspect) {
        this.subject.notifyObservers(aspect);
    }


    @Override
    public void update() {
        this.subject.update();
    }

    @Override
    public Position getCannonPosition() {
        return this.subject.getCannonPosition();
    }

    @Override
    public void moveCannonUp() {
        this.subject.moveCannonUp();
    }

    @Override
    public void gameSettings() {
        this.subject.gameSettings();
    }

    @Override
    public void nextGameMode() {
        this.subject.nextGameMode();
    }

    @Override
    public void moveCannonDown() {
        this.subject.moveCannonDown();
    }

    @Override
    public void aimCannonUp() {
        this.subject.aimCannonUp();
    }

    @Override
    public void aimCannonDown() {
        this.subject.aimCannonDown();
    }

    @Override
    public void cannonPowerUp() {
        this.subject.cannonPowerUp();
    }

    @Override
    public void cannonPowerDown() {
        this.subject.cannonPowerDown();
    }

    @Override
    public void cannonShoot() {
        this.subject.cannonShoot();
    }

    @Override
    public List<AbsMissile> getMissiles() {
        return this.subject.getMissiles();
    }

    @Override
    public List<GameObject> getGameObjects() {
        return this.subject.getGameObjects();
    }

    @Override
    public IMovingStrategy getMovingStrategy() {
        return this.subject.getMovingStrategy();
    }

    @Override
    public void toggleMovingStrategy() {
        this.subject.toggleMovingStrategy();
    }

    @Override
    public void toggleShootingMode() {
        this.subject.toggleShootingMode();
    }

    @Override
    public void increaseMissilesCount() {
        this.subject.increaseMissilesCount();
    }

    @Override
    public void decreaseMissilesCount() {
        this.subject.decreaseMissilesCount();
    }

    @Override
    public Object createMemento() {
        return this.subject.createMemento();
    }

    @Override
    public void setMemento(Object memento) {
        this.subject.setMemento(memento);
    }

    @Override
    public boolean isSettingsMode() {
        return this.subject.isSettingsMode();
    }

    @Override
    public GameSettings getSettings() {
        return this.subject.getSettings();
    }

    @Override
    public int getLevel() {
        return this.subject.getLevel();
    }

    @Override
    public IGameObjectFactory getGoFact() {
        return this.subject.getGoFact();
    }

    @Override
    public void registerCommand(AbstractGameCommand cmd) {
        this.subject.registerCommand(cmd);
    }

    @Override
    public void undoLastCommand() {
        this.subject.undoLastCommand();
    }

    @Override
    public void createControllerInfo(GameController controller) {
        this.subject.createControllerInfo(controller);
    }

    @Override
    public void reload() {
        this.subject.reload();
    }

}
