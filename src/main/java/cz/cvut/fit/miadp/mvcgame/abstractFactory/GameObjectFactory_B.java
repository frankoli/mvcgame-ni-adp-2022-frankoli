package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.IGameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.enums.EnemyType;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.Enemy1_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.Enemy2_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A.Missile_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B.Cannon_B;

public class GameObjectFactory_B implements IGameObjectFactory {
    private IGameModel model;

    @Override
    public Cannon_B createCannon() {
        return new Cannon_B(new Position(MvcGameConfig.CANNON_POS_B), this);
    }

    @Override
    public Missile_A createMissile(double initAngle, int initVelocity) {
        return new Missile_A(
                new Position(
                        model.getCannonPosition()
                ),
                initAngle,
                initVelocity,
                this.model.getMovingStrategy()
        );
    }

    @Override
    public AbsEnemy createEnemy(EnemyType type, Position initPosition) {
        if (type == EnemyType.first) {
            return new Enemy1_A(initPosition);
        }
        return new Enemy2_A(initPosition);
    }

    @Override
    public GameInfo createGameInfo(GameModel model) {
        return null;
    }

    @Override
    public AbsCollision createCollision(AbsEnemy enemy) {
        return null;
    }

}
