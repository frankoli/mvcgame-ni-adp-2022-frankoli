package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_B;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjectFactory;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.Vector;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

import java.util.ArrayList;
import java.util.List;

public class Cannon_B extends AbsCannon {
    private final IGameObjectFactory goFact;

    public Cannon_B(Position initialPosition, IGameObjectFactory goFact) {
        this.position = initialPosition;
        this.goFact = goFact;
    }

    @Override
    public void moveUp() {
        this.move(new Vector(new ArrayList<>(List.of(0, -3 * MvcGameConfig.MOVE_STEP))));
    }

    @Override
    public void moveDown() {
        this.move(new Vector(new ArrayList<>(List.of(0, 3 * MvcGameConfig.MOVE_STEP))));
    }

    @Override
    public void aimUp() {

    }

    @Override
    public void aimDown() {

    }

    @Override
    public void powerUp() {

    }

    @Override
    public void powerDown() {

    }

    @Override
    public List<AbsMissile> shoot() {
        return List.of(this.goFact.createMissile(MvcGameConfig.INIT_ANGLE, MvcGameConfig.INIT_POWER));
    }

    @Override
    public void primitiveShoot() {

    }

    @Override
    public double getAngle() {
        return 0;
    }

    @Override
    public int getPower() {
        return 0;
    }
}
