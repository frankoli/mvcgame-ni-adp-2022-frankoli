package cz.cvut.fit.miadp.mvcgame.model.gameObjects.family_A;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameInfo;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GameInfo_A extends GameInfo {
    private final GameModel model;

    public GameInfo_A(GameModel model) {
        this.model = model;
    }

    public HashMap<String, String> getInfo() {
        HashMap<String, String> result = new HashMap<>();
        result.put("Level:", Integer.toString(model.getLevel() + 1));
        result.put("Score:", Integer.toString(model.getScore()));
        Pattern p = Pattern.compile("(.*strategy\\.)(.*)(MovingStrategy*)");
        Matcher mat = p.matcher(model.getMovingStrategy().toString());
        if (mat.find())
            result.put("Moving Strategy:", mat.group(2));
        result.put("Shooting mode:", model.getCannonMode());
        result.put("Count enemies:", Integer.toString(model.getCount_enemies()));
        result.put("Missiles count:", Integer.toString(model.getMissilesCount()));
        result.put("Cannon power:", Integer.toString(model.getCannonPower()));
        double angle = Math.round(model.getCannonAngle() * 100.0) / 100.0;
        double degree = Math.round(Math.toDegrees(angle)) % 360;
        result.put("Cannon angle:", (degree + " °"));

        return result;
    }
}

